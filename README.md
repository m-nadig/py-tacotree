# Tacotree

Toolbox for data processing pipelines.

## Install

The toolbox is available on PyPI.
Install your preferred PyTorch version first, then run: `pip install tacotree`

## Usage

Knowledge of PyTorch is required. See [debug/example_model_sequential.py](debug/example_model_sequential.py) for how to
create a model in Tacotree.
