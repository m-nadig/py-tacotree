import os

from src import tacotree as tt
import torch


# Set GPU if possible
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
print('Using device', device)


def debug_function(*args):
    """
    Some function to demonstrate the Lambda module
    (for breakpoints during debugging, you can also set debug=True when creating model)
    """
    print(f'debug_function: Received {len(args)} input(s)')
    return None if len(args) == 0 else (args[0] if len(args) == 1 else args)


# ===============================================================================================
# The following section shows how to configure a model

# Sequences
# Each sequence can be regarded as a sequential model.
# Sequence names are totally arbitrary and can be any UTF-8 string.
# Note: Use separate sequence for each input for now!
sequences = {
    # Input 1 takes samples with shape (2, 100) --> e.g. a timeseries with two channels
    'input_1': [
        ('Input', dict(batch_size=None, shape=(2, 100))),
    ],

    # Input 2 takes samples with shape (1,)
    'input_2': [
        ('Input', dict(batch_size=None, shape=(1,))),
    ],

    # The backbone is a 1D CNN with global average pooling and classification layer
    'backbone': [
        # Block 1
        ('Conv1d', dict(in_channels=2, out_channels=64, kernel_size=3, padding=1)),
        ('BatchNorm1d', dict(num_features=64)),
        ('ReLU', dict(inplace=True)),
        ('Conv1d', dict(in_channels=64, out_channels=64, kernel_size=3, padding=1)),
        ('BatchNorm1d', dict(num_features=64)),
        ('ReLU', dict(inplace=True)),
        ('MaxPool1d', dict(kernel_size=2)),

        # ('Lambda', dict(fn=debug_function)),

        # Block 2
        ('Conv1d', dict(in_channels=64, out_channels=128, kernel_size=3, padding=1)),
        ('BatchNorm1d', dict(num_features=128)),
        ('ReLU', dict(inplace=True)),
        ('Conv1d', dict(in_channels=128, out_channels=128, kernel_size=3, padding=1)),
        ('BatchNorm1d', dict(num_features=128)),
        ('ReLU', dict(inplace=True)),
        ('MaxPool1d', dict(kernel_size=2)),

        # Classification head
        ('GlobalAverage', dict(n_dims=1, vectorize=True)),
        ('Linear', dict(in_features=128, out_features=1)),
    ],

    # Sigmoid layer as separate sequence
    'sigmoid': [
        ('Sigmoid',),
    ],

    # Outputs
    # Data passed to the Output module will be written into the output dictionary.
    # Here, the first one will contain logits, the second one will contain class ID.
    'output_logits': [
        ('Output',),
    ],
    'output_binary': [
        ('Output',),
    ],
}

# Workflow
# This list defines the order, how inputs are passed through the model from sequence to sequence.
# Workflows can be activated, such that only a part of the model is executed.
# If all workflows are activated (default), then the model would do the following:
#   * Pass input_1 to backbone and then to output_logits (writes data into output dictionary).
#   * Pass backbone to sigmoid and then to output_binary (writes data into output dictionary).
#   * Pass input_2 to output_logits (overwrites (!) corresponding data in output dictionary).
workflow = [
    ['input_1', 'backbone', 'output_logits'],
    ['backbone', 'sigmoid', 'output_binary'],
    ['input_2', 'output_logits'],
]

# Groups (optional)
# Can be used to group workflows (e.g., those to be activated in training and those for test sessions).
# In this example the batches will be passed the following ways:
#   * training: input_1 will be passed to backbone and then to output_logits. No further sequences are executed.
#   * test: First, input_1 will be passed to backbone and then to output_logits (just as in training). Then, the
#           output of backbone will be passed to sigmoid and then to output_binary. Finally, input_2 will be
#           passed to output_logits, where it will overwrite (!) what has be passed to output_logits from
#           backbone before.
groups = {
    'training': [0],
    'test': [0, 1, 2],
}

# Create the model
model = tt.create_model(sequences=sequences, workflow=workflow, groups=groups)

# Save and re-load the model (just for demonstration of load and save)
filename = os.path.join(os.path.dirname(__file__), 'models/test-model')
tt.save_model(model=model, filename=filename, overwrite=True, makedirs=True)
model = tt.load_model(filename=filename, debug=False)

# ===============================================================================================
# Use the model just like any other torch.nn.Module

# Move to device
model.to(device=device)

# Create some random tensors for the input
n_samples = 10
ten = torch.randn(n_samples, 2, 100).to(device=device)
ten2 = torch.ones((n_samples, 1)).to(device=device)

# Inputs should be a tacotree batch
ten = tt.as_batch_with_channels(ten)
ten2 = tt.as_batch(ten2)

# Activate workflows for training
with model.active_workflows('training'):
    # Run the model
    inputs = {
        'input_1': ten,
        'input_2': ten2,
    }
    outputs = model(**inputs)

# Alternative to the save function: Get parameters and config as a JSON
checkpoint = tt.to_storable_dict(model=model)

print(outputs)
